#!/bin/bash
RED='\033[01;31m'
GREEN='\033[01;32m'
RESET='\033[0m'
export REMOTEIP=$(/bin/grep 'REMOTEIP' /etc/cpremote/cpbackup.conf  |cut -d ':' -f2 )
export REMOTEPRT=$(/bin/grep 'REMOTEPRT' /etc/cpremote/cpbackup.conf  |cut -d ':' -f2 )
export REMOTEUSR=$(/bin/grep 'REMOTEUSR' /etc/cpremote/cpbackup.conf  |cut -d ':' -f2 )
export DESTFLDR=$(/bin/grep 'DESTFLDR' /etc/cpremote/cpbackup.conf  |cut -d ':' -f2 )

for CPUSER in `/bin/ls /etc/cpremote/clean/`
do
	echo -ne "Removing cPremote Daily Backup ..."
	ssh -F /etc/cpremote/ssh_config $REMOTEUSR@$REMOTEIP -p $REMOTEPRT rm -rf $DESTFLDR/daily/$CPUSER &
	echo -e "[ $GREEN DONE  $RESET ]"
	echo -ne "Removing cPremote Weekly Backup ..."
	ssh -F /etc/cpremote/ssh_config $REMOTEUSR@$REMOTEIP -p $REMOTEPRT rm -rf $DESTFLDR/weekly/$CPUSER &
	echo -e "[ $GREEN DONE  $RESET ]"
	echo -ne "Removing cPremote Monthly Backup ..."
	ssh -F /etc/cpremote/ssh_config $REMOTEUSR@$REMOTEIP -p $REMOTEPRT rm -rf $DESTFLDR/monthly/$CPUSER &
	echo -e "[ $GREEN DONE  $RESET ]"
done
rm -rf /etc/cpremote/clean/*
rm -rf /var/log/cpremote.*
