#!/bin/bash
RED='\033[01;31m'
GREEN='\033[01;32m'
RESET='\033[0m'
echo -ne "Removing cPremote .."
rm -rf /usr/local/cpanel/whostmgr/docroot/cgi/addon_cPremote.cgi
rm -rf /usr/local/cpanel/whostmgr/docroot/cgi/cpremote
/usr/bin/crontab -l | grep -v 'cpremotebackup' > /tmpa.cron 
cat /tmpa.cron > /var/spool/cron/root
rm -f /tmpa.cron
rm -f /scripts/cpremote.php
rm -f /scripts/cpremotebackup
rm -rf /scripts/cpremote*
rm -rf /scripts/prekillacct
rm -rf /etc/cron.weekly/updatecpremote.sh
rm -rf /etc/cpremote/*
rm -rf /etc/cpremote

echo -e "[ $GREEN done $RESET ]"	
