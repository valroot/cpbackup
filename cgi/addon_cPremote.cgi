#!/usr/bin/perl
#WHMADDON:cPremote:cPanel Remote Backup
# start main

use File::Find;
use Fcntl qw(:DEFAULT :flock);
use Sys::Hostname qw(hostname);
use IPC::Open3;

open (IN, "</etc/cpremote/version.txt") or die $!;
$myv = <IN>;
close (IN);
chomp $myv;

$script = "addon_cPremote.cgi";
$images = "cpremote";

use lib '/usr/local/cpanel';
use Cpanel::cPanelFunctions ();
use Cpanel::Form            ();
use Cpanel::Config          ();
use Cpanel::Version          ();
use Whostmgr::HTMLInterface ();
use Whostmgr::ACLS          ();

Whostmgr::ACLS::init_acls();

print "Content-type: text/html\r\n\r\n";

if (!Whostmgr::ACLS::hasroot()) {
        print "You do not have access to cPremote .\n";
        exit();
}

eval ('use Cpanel::Rlimit                       ();');
unless ($@) {Cpanel::Rlimit::set_rlimit_to_infinity()}

$Cpanel::App::appname = "whostmgr";
Whostmgr::HTMLInterface::defheader("cPanel Remote Backup" ,'/cgi/addon_cPremote.cgi');

%FORM = Cpanel::Form::parseform();

$dns = Cpanel::Version::gettree();
if ($dns eq "DNSONLY") {$dns = 1} else {$dns = 0}

print "<meta http-equiv=\"refresh\" content=\"0;url=cpremote/index.php\"/>" ;
1;
