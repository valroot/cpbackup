<?php
print('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css"></link>
	<link rel="STYLESHEET" type="text/css" href="./dhtmlx/style/dhtmlxgrid.css">
	<link rel="STYLESHEET" type="text/css" href="./dhtmlx/style/dhtmlxgrid_pgn_bricks.css">
	
	<script  src="./dhtmlx/js/dhtmlxcommon.js"></script>
	<script  src="./dhtmlx/js/dhtmlxgrid.js"></script>		
	<script  src="./dhtmlx/js/ext/dhtmlxgrid_pgn.js"></script>	
	<script  src="./dhtmlx/js/dhtmlxgridcell.js"></script>
	<script  src="./dhtmlx/js/ext/dhtmlxgrid_srnd.js"></script>
	<script  src="./dhtmlx/js/ext/dhtmlxgrid_filter.js"></script>	

</head>
');
print('
<div  id="contentimg"><a href="view.php"><img src="./images/listbkp.png" alt="alt" border="0"/></a><br/><a href="view.php" >List Backup Status </a></div>
<div style="clear:both"></div>
<div class="content3">
<body id="mainbody">
<div id="middiv"></div>
<div id="gridbox" width="405px" height="610px" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea" style="overflow:hidden" >
<script>	
		var mygrid = new dhtmlXGridObject(\'gridbox\');
		mygrid.setImagePath("./dhtmlx/imgs/");
		mygrid.setHeader("SL.NO,cPanel User Name, Backup Status");
		mygrid.setInitWidths("55,200,150");
		mygrid.attachHeader(" ,#text_filter,#text_filter,#text_filter, , , , ");
		mygrid.setColAlign("center,left,left");
		mygrid.setColTypes("ro,ro,ro");
		mygrid.setColSorting("str,str,str");
		mygrid.enablePaging(true,24,3,"pagingArea",true);
		mygrid.setSkin("modern");
		mygrid.setPagingSkin("bricks");		
		mygrid.init();
		dhtmlxError.catchError("LoadXML", function(){ return false; });
		mygrid.loadXML("view_1.php");			
</script>
</div>
</div>
</div>
');
 include('footer.php');
?>
</div>

