<?php

function getlicense(){
$line="";
$flines=file("/etc/cpremote/id_rsa.pub");
foreach ($flines as $line_num => $line){ 
 	echo  "$line";  
	}
}

print('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css"></link>
</head>
<body>
<div  id="contentimg"><a href="public_key.php"><img src="./images/key.png" alt="alt" border="0"/></a><br/><a href="public_key.php" >Your Backup Public Key</a></div>

<div class="content1">
');
?>
<p>
<strong>Your Backup Public Key</strong> <br /><br />
You can use the following publick key for configuring ssh access to your remote backup account. <br /><br />

<textarea rows="10" cols="100" readonly>
<?php
getlicense();
?>
</textarea>
<br /><br />
<strong>How to setup your remote backup ssh account. ?</strong><br />
Please do the following steps.
<br ><ul>
<li> SSH to your remote server  using the user name and password of your backup ssh account<br /> ( eg : backupusr@10.0.0.10  , where 10.0.0.10 is the remote backup server IP )</li>
<li>Create  the following folder and file </li>
	<pre>
	# mkdir .ssh 
	# touch .ssh/authorized_keys
	# chmod 750 .ssh/
	# chmod 600 .ssh/authorized_keys
</pre>
<li> Now copy the above showing <b> Backup Public Key</b> to the file .ssh/authorized_keys </li>
</ul>
This is all about the configurations. 
</p>

<?php include('footer.php'); ?>
</div>

