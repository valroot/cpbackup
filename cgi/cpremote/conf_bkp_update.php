<?php
print('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css"></link>
</head>
<body>
<div class="headcontent">');
include('menu.php');
print('      
</div>
<div class="content">
');
if(isset($_POST['key_hname'])){
     //   print_r($_POST);
        $remote_host=trim($_POST['key_hname']);
        $remote_user=trim($_POST['key_user']);
        $remote_port=trim($_POST['key_port']);
        $remote_folder=trim($_POST['key_folder']);
        $backup_option=trim($_POST['key_option']);
        $backup_daily=trim($_POST['key_daily']);
        $backup_weekly=trim($_POST['key_weekly']);
        $backup_monthly=trim($_POST['key_monthly']);
        $backup_notice=trim($_POST['key_notice']);
	$flines=file("/etc/cpremote/cpbackup.conf");
	foreach ($flines as $line_num => $line){ 
        	if((preg_match("/^REMOTEIP/",$line)))
             		$flines[$line_num]="REMOTEIP:".$remote_host."\n";
             	if((preg_match("/^REMOTEUSR/",$line)))
             		$flines[$line_num]="REMOTEUSR:".$remote_user."\n";
             	if((preg_match("/^REMOTEPRT/",$line)))
             		$flines[$line_num]="REMOTEPRT:".$remote_port."\n";
             	if((preg_match("/^DESTFLDR/",$line)))
             		$flines[$line_num]="DESTFLDR:".$remote_folder."\n";
             	if((preg_match("/^STATUS/",$line)))
             		$flines[$line_num]="STATUS:".$backup_option."\n";
             	if((preg_match("/^BKPDAILY/",$line)))
             		$flines[$line_num]="BKPDAILY:".$backup_daily."\n";
             	if((preg_match("/^BKPWKLY/",$line)))
             		$flines[$line_num]="BKPWKLY:".$backup_weekly."\n";
             	if((preg_match("/^BKPMONTHLY/",$line)))
             		$flines[$line_num]="BKPMONTHLY:".$backup_monthly."\n";
             	if((preg_match("/^NOTIFICATION/",$line)))
             		$flines[$line_num]="NOTIFICATION:".$backup_notice."\n";
	}
	$lnum=count($flines);
   	$fd=fopen("/etc/cpremote/cpbackup.conf",'w');
   	for($i=0;$i< $lnum;$i++){
       		fwrite($fd,$flines[$i]);
       	}
  	fclose($fd);
	echo '<font color="green">Backup configuration updated successfully !!! </font>';	

}else{
        echo '<font color="red">There is no value for update please <a href="conf_bkp.php">click here</a> </font>';
}
?>
<p>

</p>
<?php include('footer.php'); ?>
</div>

