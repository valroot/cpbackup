<?php
print('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css"></link>
</head>
<body>
<div  id="contentimg"><a href="conf_bkp.php"><img src="./images/configure.png" alt="alt" border="0"/></a><br/><a href="conf_bkp.php" >Configure Backup</a></div>

<div class="content1">
');

?>
<?php
$lines = file("/etc/cpremote/cpbackup.conf");
foreach ($lines as $key) {
$term = explode(":",trim($key));
        if($term[0]=='REMOTEIP'){
        		$key_remote_ip=$term[1];
       		}
	elseif( $term[0]=='REMOTEUSR') {
			$key_remote_user=$term[1];
		}
	elseif($term[0]=='REMOTEPRT' ) {
			$key_remote_port=$term[1];
		}
	elseif($term[0]=='DESTFLDR' ) {
		//$secon_value=explode("/",$term[1]);
			$key_restination_folder=$term[1];
			//print_r($key_restination_folder);
		}
	elseif($term[0]=='RSYNCPTH' ) {
			$key_rsync_path=$term[1];
		}
	elseif($term[0]=='STATUS' ) {
			$key_bkp_status=$term[1];
		}
	elseif($term[0]=='BKPWKLY' ) {
			$key_bkp_weekly=$term[1];
		}

	elseif($term[0]=='BKPMONTHLY' ) {
			$key_bkp_monthly=$term[1];

		}
	elseif($term[0]=='BKPDAILY' ) {
			$key_bkp_daily=$term[1];
		}
	elseif($term[0]=='NOTIFICATION' ) {
			$key_bkp_notification=$term[1];
		}
	else 
		echo "";

	
}
?>
<p>
<table border="1" width="100%" color="black">                             
	</table>
</p>
<p>
<form action="conf_bkp_update.php" method="post">
<table border="1" width="100%"  cellspacing="0">
<tr height="40px"  bgcolor="#EBF3F6">
<td width="400px" align="center"><font size="2"><strong>Feature</strong></font><br></td>
<td width="600px" align="center"><font size="2"><strong>Value</font><br></td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Remote Backup Server SSH IP<br /></td>
<td width="600px" align="left"><input type="text" name="key_hname" size="20" value="<?php echo $key_remote_ip; ?>">(eg : 192.168.1.2)</td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Remote SSH Username<br /></td>
<td width="600px" align="left"><input type="text" name="key_user" size="20" value="<?php echo $key_remote_user;  ?>">( A remote shell user , eg:bkpusr)</td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Remote SSH Port <br /></td>
<td width="600px" align="left"><input type="text" name="key_port" size="20" value="<?php echo $key_remote_port;  ?>">(Open this TCP port in firewall)<br  /></td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Remote SSH User Home<br /></td>
<td width="600px" align="left"><input type="text" name="key_folder" size="20" value="<?php echo $key_restination_folder; ?>">(No ending  '/' . eg: /home/cpremote)<br /></td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Enable  Backup<br /></td>
<td width="600px" align="left">
<select name="key_option">
<?php
if($key_bkp_status == '0' ){
	print('<option value="0" selected="selected">No(Default)</option> ');}
else{
	print('<option value="1" selected="selected">Yes(Default)</option> ');}
?>
<option value="1">Yes</option>
<option value="0">No</options></select><br /></td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Enable Daily Backup <br /></td>
<td width="600px" align="left">
<select name="key_daily">
<?php
if($key_bkp_daily == '0' ){
	print('<option value="0" selected="selected">No(Default)</option> ');}
else{
	print('<option value="1" selected="selected">Yes(Default)</option> ');}
?>
<option value="1">Yes</option>
<option value="0">No</options>
</select>
</tr>
<br /></td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Enable Weekly Backup<br /></td>
<td width="600px" align="left">
<select name="key_weekly">
<?php
if($key_bkp_weekly == '0' ){
	print('<option value="0" selected="selected">No(Default)</option> ');}
elseif($key_bkp_weekly == '1'){
	print('<option value="1" selected="selected">Monday(Default)</option> ');}
elseif($key_bkp_weekly == '2'){
	print('<option value="2" selected="selected">Tuesday(Default)</option> ');}
elseif($key_bkp_weekly == '3'){
	print('<option value="3" selected="selected">Wednesday(Default)</option> ');}
elseif($key_bkp_weekly == '4'){
	print('<option value="4" selected="selected">Thursday(Default)</option> ');}
elseif($key_bkp_weekly == '5'){
	print('<option value="5" selected="selected">Friday(Default)</option> ');}
elseif($key_bkp_weekly == '6'){
	print('<option value="6" selected="selected">Saturday(Default)</option> ');}
elseif($key_bkp_weekly == '7'){
	print('<option value="7" selected="selected">Sunday(Default)</option> ');}

else{
	print('<option value="0" selected="selected">No(Default)</option> ');}

?>
<option value="0">No</option>
<option value="1">Monday</options>
<option value="2">Tuesday</options>
<option value="3">Wednesday</options>
<option value="4">Thursday</options>
<option value="5">Friday</options>
<option value="6">Saturday</options>
<option value="7">Sunday</options>
</select>
<br /></td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Enable Monthly Backup<br /></td>
<td width="600px" align="left">
<select name="key_monthly">
<?php
if($key_bkp_monthly == '0' ){
	print('<option value="0" selected="selected">No(Default)</option> ');}
elseif($key_bkp_monthly == '1'){
	print('<option value="1" selected="selected">Yes(Default)</option> ');}
else{
	print('<option value="0" selected="selected">No(Default)</option> ');
	}
?>
<option value="0">No</option>
<option value="1">Yes</options>
</select>
<br /></td> 
</tr>
<tr height="30px">
<td width="400px" align="left">Enable Email Notification<br /></td>
<td width="600px" align="left">
<select name="key_notice">
<?php
if($key_bkp_notification == '0' ){
	print('<option value="0" selected="selected">No(Default)</option> ');}
elseif($key_bkp_notification == '1'){
	print('<option value="1" selected="selected">Yes(Default)</option> ');}
else{
	print('<option value="1" selected="selected">Yes(Default)</option> ');
	}
?>
<option value="1">Yes</option>
<option value="0">No</options>
</select>(cPanel/WHM Setup > Server Contact E-Mail Address)
<br /></td> 
</tr>
</tr>
</table><br >
<input type="submit" value="Update" />
</form>


</p>
<?php include('footer.php'); ?>
</div>

