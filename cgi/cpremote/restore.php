<?php
function restorebkp($cmd){
$handle = popen("$cmd", 'r');
while(!feof($handle)) {
    $buffer = fgets($handle);
    echo "$buffer<br/>\n";
    ob_flush();
    flush();
    }
pclose($handle);
}

print('<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css"></link>
	<script>
		window.dhx_globalImgPath="./dhtmlx/imgs/";
	</script>
	<link rel="STYLESHEET" type="text/css" href="./dhtmlx/style/dhtmlxcombo.css">
	<script src="./dhtmlx/js/dhtmlxcombo.js" type="text/javascript"></script>
	<script  src="./dhtmlx/js/ext/dhtmlxcombo_extra.js"></script>
	<script  src="./dhtmlx/js/dhtmlxcommon.js"></script>		
</head>
<div class="headcontent">');
$msg="";
print('      
</div>
<div  id="contentimg"><a href="restore.php"><img src="./images/restore.png" alt="alt" border="0"/></a><br/><a href="restore.php" >Restore Files</a></div>

<div class="content1">
<body id="mainbody">
<div id="content">
<textarea rows="6" cols="100" readonly disabled >
You can restore backups from here.  You can chose daily, weekly or monthly backups as per your configurations. The following options are available.
Backup Type Email : This will restore the email folder of the cPanel user
Backup Type Home Folder : This will restore the users full home folder which contains emails and documents.
Backup Type Document Root : This  will restore public_html folder only.
</textarea><br /></br >
');
if ( file_exists("/etc/cpremote/status/daily.log") || file_exists("/etc/cpremote/status/weekly.log") || file_exists("/etc/cpremote/status/monthly.log") ){
	
print('
<div><i>Warning : If you are going to restore big account, we recommend to restore it form the server shell, Please see the documentation from, <a href="http://wiki.sysvm.com/Restore_cPremote_Backups">Restore cPremote Backup</a></i> </div><br />
<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="return check();">
<div><font size="2"><b>Select A cPanel User Name</b></font></div> <br />
<div id="combo_zone" style="width:280px; height:30px;float:left;"></div>
<div id="message" class="msgstyle" >
&nbsp;'.$msg.'
</div>
<br />

<div id="textstyle" >&nbsp;</div>
<div>Select Restore From : &nbsp;
	<select name="bkp_from" >
');
	if ( file_exists("/etc/cpremote/status/daily.log")){
	$lines = file("/etc/cpremote/status/daily.log");
		foreach ($lines as $key){
		echo '<option value="daily">Daily ('.$key.')</option>';
		}
	}
	if(file_exists("/etc/cpremote/status/weekly.log")){
	$lines = file("/etc/cpremote/status/weekly.log");
		foreach ($lines as $key){
		echo '<option value="weekly">Weekly ('.$key.')</option>';
		}
	}
	if(file_exists("/etc/cpremote/status/monthly.log")){
		$lines = file("/etc/cpremote/status/monthly.log");
		foreach ($lines as $key){
		echo '<option value="monthly">Monthly ('.$key.')</option>';
		}
	}
print('
	</select>
 </div> <br />
<div>Select Backup Type  : &nbsp;
	<select name="bkp_type">
	<option value="mail">Email</option>
	<option value="homefolder">Home Folder</options>
	<option value="www">Document Root</options>
	</select>
</div><br />

<div><input type="submit" class="button" name="submit" value="Restore" /></div>
<br />	
</form>
');
}else{
echo "Please update your backup. It seems the backup is not updated till";
}
print('
</div>
<script>		
		       
        var z=new dhtmlXCombo("combo_zone","alfa2",256);
        z.readonly(1)
        z.loadXML("restore_1.php");
			function check(){
		if(z.getSelectedValue()==null)
		{	
		var msg="<font color=\"red\">Please Select a cPanel User</font>";
		document.getElementById(\'message\').innerHTML=msg;
		return false;		
		}	}
</script>
</body>
');
if(isset($_POST['submit']))
{
//print_r($_POST);
$user=trim($_POST['alfa2']);
$type=trim($_POST['bkp_type']);
$folder=trim($_POST['bkp_from']);
$cmd="/scripts/cpremoterestore --user=$user --folder=$folder --type=$type";
//echo $cmd;
restorebkp($cmd);
}
include('footer.php');
print('
</div>
');
?>
