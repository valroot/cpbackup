#!/bin/bash
RED='\033[01;31m'
GREEN='\033[01;32m'
RESET='\033[0m'
RED='\033[01;31m'
GREEN='\033[01;32m'
RESET='\033[0m'
clear
echo -e "$GREEN*************************************************************$RESET"
echo -e "          Installation Wizard"
echo -e "          v.5.4.1:"
echo -e "$GREEN*************************************************************$RESET"
echo " "
echo " "
echo -ne "Checking for cPanel .."
if [ -e  "/usr/local/cpanel/version" ]; then
	echo -e "[ $GREEN YES $RESET ]"
else
	echo -e "[ $RED No $RESET ]"
	exit
fi

echo -ne "Checking for previous installation .."
if [ -e  "/etc/cpremote/cpbackup.conf" ]; then
	echo -e "[ $GREEN FOUND $RESET ]"
	rm -rf /usr/local/cpanel/whostmgr/docroot/cgi/cpremote
	rm -f /scripts/cpremotebackup
	rm -rf /scripts/cpremote/
	rm -f /scripts/cpremote.php
	rm -f /etc/cpremote/version.txt
	rm -f /etc/cron.weekly/updatecpremote.sh
	rm -f /scripts/prekillacct
	chown root.root cgi/* -R
	chown root.root etc/* -R
	chown root.root scripts/* -R
	cp -arf cgi/* 	/usr/local/cpanel/whostmgr/docroot/cgi/
	cp -f etc/cpremote/version.txt /etc/cpremote/
	cp -f etc/cpremote/userhomes.txt /etc/cpremote/
	if [ -e "/etc/cpremote/php.ini" ]; then
		echo "cPremote php.ini seems ok"
	else
		cp -vf  /usr/local/lib/php.ini /etc/cpremote/
		sed -i 's/disable_functions.*/;disable_functions/g' /etc/cpremote/php.ini
		sed -i 's/safe_mode.*/safe_mode = Off/g' /etc/cpremote/php.ini
	fi
	mkdir -p /etc/cpremote/status
		if [ -f "/etc/cpremote/id_rsa" ];then
			echo -ne "Checking for ssh keys "
			echo -e "[ $GREEN YES $RESET ]"
		else
			ssh-keygen -t rsa -f /etc/cpremote/id_rsa -N ""
		fi


	chmod 700 scripts/*
	cp -arf scripts/* /scripts/
	echo -ne "Checking for autoupdate cron"
	if [ -e "/etc/cron.weekly/updatecpremote.sh" ] ; then
 		echo -e "[ $GREEN YES $RESET ]"
	else
        	echo -e "[ $RED No $RESET ]"
        	cp -v  updatecpremote.sh  /etc/cron.weekly/
        	chmod 700 /etc/cron.weekly/updatecpremote.sh
	fi
	cp -fv  clearcpremote.sh /etc/cron.weekly/
	chmod 750 /etc/cron.weekly/clearcpremote.sh
        mkdir -pv /etc/cpremote/clean/ 
	echo -ne "Checking for Rsync 3.0 ............."
	if [ -e  "/usr/bin/rsync" ]; then
        	echo -e "[ $GREEN YES $RESET ]"
	else
        	echo -e "[ $RED No $RESET ]"
        	echo "Installing Rsync 3.0...."
        	wget -c http://valroot.com/cpbackup/rsync-3.0.9.tar.gz
        	tar -xzf rsync-3.0.9.tar.gz
        	cd rsync-3.0.9/
        	./configure --prefix=/opt/rsync
       	 	make
        	make install
        	cd ..
        	rm -rf rsync*
	fi
	echo -ne "Checking for cpulimit"
	if [ -e "/usr/sbin/cpulimit" ] ; then
        	echo -e "[ $GREEN YES $RESET ]"
	else
        	echo -e "[ $RED No $RESET ]"
        	echo "Installing cpulimit"
        	wget -c http://valroot.com/cpbackup/cpulimit-1.1.tar.gz
        	tar -xzf cpulimit-1.1.tar.gz
        	cd cpulimit-1.1/
        	make
        	cp -af cpulimit /usr/sbin/
        	cd ..
        	rm -rf cpulimit*
	fi
	#sed -i 's/RSYNCPTH.*/RSYNCPTH:\/opt\/rsync\/bin\/rsync/g' /etc/cpremote/cpbackup.conf
 	echo -ne "Upgrade Completed .."
	echo -e "[ $GREEN OK $RESET ]"
	exit 0
fi
echo -ne "Checking for Yum .."
if [ -e  "/etc/yum.conf" ]; then
	echo -e "[ $GREEN YES $RESET ]"
	yum -y install mailx
else
	echo -e "[ $RED No $RESET ]"
	exit
fi
echo -ne "Checking for SSH .."
if [ -e  "/usr/bin/ssh" ]; then
	echo -e "[ $GREEN YES $RESET ]"
else
	echo -e "[ $RED No $RESET ]"
	exit
fi
echo -ne "Checking for RSYNC .."
if [ -e  "/usr/bin/rsync" ]; then
	echo -e "[ $GREEN YES $RESET ]"
else
	echo -e "[ $RED No $RESET ]"
	exit
fi
echo -ne "Checking for SED .."
if [ -e  "/bin/sed" ]; then
	echo -e "[ $GREEN YES $RESET ]"
else
	echo -e "[ $RED No $RESET ]"
	exit
fi
echo -ne "Checking for GREP .."
if [ -e  "/bin/grep" ]; then
	echo -e "[ $GREEN YES $RESET ]"
else
	echo -e "[ $RED No $RESET ]"
	exit
fi
echo -ne "Checking for /usr/local/bin/php .."
if [ -e  "/usr/local/bin/php" ]; then
        echo -e "[ $GREEN YES $RESET ]"
else
        echo -e "[ $RED NO $RESET ]"
	echo -e "$RED I can't find /usr/local/bin/php . Please use the script /scripts/easyapache to rebuild PHP , after that rerun this installation script $RESET";
        exit
fi


echo -ne "Disabling cPanel default Backup options .."
	sed -i 's/BACKUPENABLE.*/BACKUPENABLE no/g'  /etc/cpbackup.conf
	echo -e "[ $GREEN YES $RESET ]"

echo -ne "Installing cPremote Plugin..."
	chown root.root cgi/* -R
	chown root.root etc/* -R
	chown root.root scripts/* -R
	chmod 700 cgi/addon_cPremote.cgi
	cp -arf cgi/* 	/usr/local/cpanel/whostmgr/docroot/cgi/
	cp -arf etc/* /etc/
	ssh-keygen -t rsa -f /etc/cpremote/id_rsa -N ""
	chmod 700 scripts/*
	cp -arf scripts/*  /scripts/
	echo -e "[ $GREEN done $RESET ]"	
echo -ne "Installing backup cron .."
	echo "0 1 * * * /scripts/cpremotebackup > /dev/null 2>&1 " >> /var/spool/cron/root
	echo -e "[ $GREEN done $RESET ]"
# install update cron

echo -ne "Checking for autoupdate cron"
if [ -e "/etc/cron.weekly/updatecpremote.sh" ] ; then

	echo -e "[ $GREEN YES $RESET ]"
else
	echo -e "[ $RED No $RESET ]"
	cp -v  updatecpremote.sh  /etc/cron.weekly/
	chmod 700 /etc/cron.weekly/updatecpremote.sh
fi

# Setup weekly clean cron

cp -v clearcpremote.sh /etc/cron.weekly/
chmod 750 /etc/cron.weekly/clearcpremote.sh

# Installing cpulimit
echo -ne "Checking for cpulimit"
if [ -e "/usr/sbin/cpulimit" ] ; then
	echo -e "[ $GREEN YES $RESET ]"
else
	echo -e "[ $RED No $RESET ]"
	echo "Installing cpulimit"
	wget -c http://download.sysvm.com/deps/cpulimit-1.1.tar.gz
	tar -xzf cpulimit-1.1.tar.gz
	cd cpulimit-1.1/
	make
	cp -af cpulimit /usr/sbin/
	cd ..
	rm -rf cpulimit*
fi
# Install rsync 3.0
echo -ne "Checking for Rsync 3.0.9 ............."

if [ -e  "/opt/rsync/bin/rsync" ]; then
        echo -e "[ $GREEN YES $RESET ]"

else
        echo -e "[ $RED No $RESET ]"
        echo "Installing Rsync 3.0...."
        wget -c http://valroot.com/cpbackup/rsync-3.0.9.tar.gz
        tar -xzf rsync-3.0.9.tar.gz
        cd rsync-3.0.9/
        ./configure --prefix=/opt/rsync
        make
        make install
	cd ..
	rm -rf rsync*
fi

echo -ne "Checking for Ioncube Loader..."
if /usr/local/bin/php -v | grep 'ionCube' > /dev/null ;then
	echo -e "[ $GREEN YES $RESET ]"
else
	/scripts/phpextensionmgr install IonCubeLoader
fi

cp -vf  /usr/local/lib/php.ini /etc/cpremote/
sed -i 's/disable_functions.*/;disable_functions/g' /etc/cpremote/php.ini
sed -i 's/safe_mode.*/safe_mode= Off/g' /etc/cpremote/php.ini

echo -e "--------------------------------------------------------------------------------------"
echo -e "				Installation Completed"
echo -e "	Please configure your backup from WHM->Plugins->cPanel Remote Backup"
echo -e "--------------------------------------------------------------------------------------"
