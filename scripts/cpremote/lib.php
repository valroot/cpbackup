<?php

function uptodate( $fname, $data )
{
    $fh = fopen( $fname, "w" );
    fwrite( $fh, $data );
    fclose( $fh );
}

function notification( $to, $host )
{
    $message = "Hello,".( "\n" )."    cPremote Backup  cron completed on your server. This does not mean your backup completed.  You may please verify the complete backup log from /var/log/cpremote.".date( "Y-m-d" );
    $headers = "From: cpanel@"."{$host}"."\r\n"."Reply-To: webmaster@"."{$host}"."\r\n"."X-Mailer: PHP/".phpversion( );
    mail( $to, $subject, $message, $headers );
}

function check_license( $licensekey, $localkey )
{
    $chy = curl_init( );
    curl_setopt( $chy, CURLOPT_URL, "http://valroot.com/cpbackup/index.php" );
    curl_setopt( $chy, CURLOPT_RETURNTRANSFER, 1 );
    $ip = curl_exec( $chy );
    $HIP = $ip;
    curl_close( $chy );
    $whmcsurl = "http://valroot.com/";
    $licensing_secret_key = "cs+Sda1Qdsfx4pc4xebwzkbf";
    $checkdate = date( "Ymd" );
    $usersip = $ip;
    $localkeydays = 7;
    $allowcheckfaildays = 2;
    $localkeyvalid = false;
    if ( $localkey )
    {
        $localkey = str_replace( "\n", "", $localkey );
        $localdata = substr( $localkey, 0, strlen( $localkey ) - 32 );
        if ( $Var_1344 == md5( $localdata.$licensing_secret_key ) )
        {
            $localdata = strrev( $localdata );
            $md5hash = substr( $localdata, 0, 32 );
            $localdata = substr( $localdata, 32 );
            $localdata = base64_decode( $localdata );
            $localkeyresults = unserialize( $localdata );
            $originalcheckdate = $localkeyresults['checkdate'];
            if ( $md5hash == md5( $originalcheckdate.$licensing_secret_key ) )
            {
                $localexpiry = date( "Ymd", mktime( 0, 0, 0, date( "m" ), date( "d" ) - $localkeydays, date( "Y" ) ) );
                if ( $localexpiry < $originalcheckdate )
                {
                    $localkeyvalid = true;
                    $results = $localkeyresults;
                    $validdomains = explode( ",", $results['validdomain'] );
                    if ( !in_array( $_SERVER['SERVER_NAME'], $validdomains ) )
                    {
                        $localkeyvalid = false;
                        $localkeyresults['status'] = "Active";
                        $results = array( );
                    }
                    $validips = explode( ",", $results['validip'] );
                    if ( !in_array( $usersip, $validips ) )
                    {
                        $localkeyvalid = false;
                        $localkeyresults['status'] = "Active";
                        $results = array( );
                    }
                    if ( $results['validdirectory'] != dirname( __FILE__ ) )
                    {
                        $localkeyvalid = false;
                        $localkeyresults['status'] = "Active";
                        $results = array( );
                    }
                }
            }
        }
    }
    if ( !$localkeyvalid )
    {
        if ( isset( $_SERVER['HOSTNAME'] ) )
        {
            $hostname = trim( $_SERVER['HOSTNAME'] );
        }
        else
        {
            $hostname = "localhost";
        }
        $postfields['licensekey'] = $licensekey;
        $postfields['domain'] = $hostname;
        $postfields['ip'] = $HIP;
        $postfields['dir'] = "/usr/local/cpanel/whostmgr/docroot/cgi/cpremote";
        if ( function_exists( "curl_exec" ) )
        {
            $ch = curl_init( );
            curl_setopt( $ch, CURLOPT_URL, $whmcsurl."modules/servers/licensing/verify.php" );
            curl_setopt( $ch, CURLOPT_POST, 1 );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $postfields );
            curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            $data = curl_exec( $ch );
            curl_close( $ch );
        }
        else
        {
            $fp = fsockopen( $whmcsurl, 80, $errno, $errstr, 5 );
            $querystring = "";
            foreach ( $postfields as $k => $v )
            {
                $querystring .= "{$k}=".urlencode( $v )."&";
            }
            $header = "POST ".$whmcsurl."modules/servers/licensing/verify.php HTTP/1.0\r\n";
            $header .= "Host: ".$whmcsurl."\r\n";
            $header .= "Content-type: application/x-www-form-urlencoded\r\n";
            $header .= "Content-length: ".@strlen( @$querystring )."\r\n";
            $header .= "Connection: close\r\n\r\n";
            $header .= $querystring;
            $data = "";
            @stream_set_timeout( @$fp, 20 );
            @fputs( @$fp, @$header );
            $status = @socket_get_status( @$fp );
            while ( !feof( @$fp ) && $status )
            {
                $data .= @fgets( @$fp, 1024 );
                $status = @socket_get_status( @$fp );
            }
            @fclose( @$fp );
        }
    }
    if ( !$data )
    {
        $localexpiry = date( "Ymd", mktime( 0, 0, 0, date( "m" ), date( "d" ) - ( $localkeydays + $allowcheckfaildays ), date( "Y" ) ) );
        if ( $localexpiry < $originalcheckdate )
        {
            $results['status'] = "Active";
            return $results;
        }
        else
        {
            $results['status'] = "Active";
            return $results;
        }
    }
    preg_match_all( "/<(.*?)>([^<]+)<\\/\\1>/i", $data, $matches );
    $results = array( );
    foreach ( $matches[1] as $k => $v )
    {
        $results[$v] = $matches[2][$k];
    }
    if ( $results['status'] == "Active" )
    {
        $results['checkdate'] = $checkdate;
        $data_encoded = serialize( $results );
        $data_encoded = base64_encode( $data_encoded );
        $data_encoded = md5( $checkdate.$licensing_secret_key ).$data_encoded;
        $data_encoded = strrev( $data_encoded );
        $data_encoded = $data_encoded.md5( $data_encoded.$licensing_secret_key );
        $data_encoded = wordwrap( $data_encoded, 80, "\n", true );
        $results['localkey'] = $data_encoded;
    }
    $results['remotecheck'] = true;
}
unset( $postfields );
unset( $data );
unset( $matches );
unset( $whmcsurl );
unset( $licensing_secret_key );
unset( $checkdate );
unset( $usersip );
unset( $localkeydays );
unset( $allowcheckfaildays );
unset( $md5hash );
return $results;
?>