<?php

include( "lib.php" );
$lines = file( "/etc/cpremote/cpbackup.conf" );
foreach ( $lines as $key )
{
    $term = explode( ":", trim( $key ) );
    if ( $term[0] == "LICENSE" )
    {
        $key = $term[1];
        break;
        break;
    }
}
$licensekey = $key;
$lineskey = file( "/etc/cpremote/localkey.txt" );
$localkey = NULL;
foreach ( $lineskey as $key )
{
    $localkey .= $key;
}
$fvar10 = file( "/etc/wwwacct.conf" );
$IP = explode( " ", trim( $fvar10[0] ) );
$results['status'] = "Active";
if ( $results['status'] == "Active" )
{
    if ( $argc != 4 )
    {
        echo "\n\nUsage:\n \n--help   - Show this  menu\n--user=USER    , cpanel user\n--folder=daily|weekly|monthly   , backup retension folder\n--type=mail|homefolder|www|full    ,  whcih folder need to restore  ,full mean full cpanel restore\n\n\n";
    }
    else
    {
        if ( !file_exists( "/var/log/cpremoterestore.log" ) )
        {
            $fh = fopen( "/var/log/cpremoterestore.log", "w" );
            fwrite( $fh, "cpremote restore logs\n" );
            fclose( $fh );
        }
        $userdata = array( );
        foreach ( $argv as $val )
        {
            if ( preg_match( "/^--user=/", $val ) )
            {
                $ud = explode( "--user=", $val );
                $userdata['USERNAME'] = trim( $ud[1] );
            }
            if ( preg_match( "/^--folder=/", $val ) )
            {
                $ud = explode( "--folder=", $val );
                $userdata['FOLDER'] = trim( $ud[1] );
            }
            if ( preg_match( "/^--type=/", $val ) )
            {
                $ud = explode( "--type=", $val );
                $userdata['TYPE'] = trim( $ud[1] );
            }
        }
        $backupconf = array( );
        $flineconf = file( "/etc/cpremote/cpbackup.conf" );
        foreach ( $flineconf as $val )
        {
            if ( preg_match( "/^REMOTEIP/", $val ) )
            {
                $cd = explode( ":", $val );
                $backupconf['REMOTEIP'] = trim( $cd[1] );
            }
            if ( preg_match( "/^REMOTEUSR/", $val ) )
            {
                $cd = explode( ":", $val );
                $backupconf['REMOTEUSR'] = trim( $cd[1] );
            }
            if ( preg_match( "/^REMOTEPRT/", $val ) )
            {
                $cd = explode( ":", $val );
                $backupconf['REMOTEPRT'] = trim( $cd[1] );
            }
            if ( preg_match( "/^DESTFLDR/", $val ) )
            {
                $cd = explode( ":", $val );
                $backupconf['DESTFLDR'] = trim( $cd[1] );
            }
            if ( preg_match( "/^RSYNCPTH/", $val ) )
            {
                $cd = explode( ":", $val );
                $backupconf['RSYNCPTH'] = trim( $cd[1] );
            }
        }
        $hlines = file( "/etc/cpremote/home.list" );
        $userhome = array( );
        $userphome = array( );
        foreach ( $hlines as $key )
        {
            $term = explode( ":", trim( $key ) );
            $userhome[$term[0]] = $term[1];
            $term2 = explode( "/", $userhome[$term[0]] );
            $userphome[$term[0]] = trim( $term2[1] );
        }
        if ( $userdata['TYPE'] == "mail" )
        {
            $logmsg = date( "r" )." : Restoring Mail folders for the user ".$userdata['USERNAME']." from ".$userdata['FOLDER']." backup \n";
            error_log( "{$logmsg}", 3, "/var/log/cpremoterestore.log" );
            $restorecmd = $Tmp_253.$backupconf['REMOTEUSR']."@".$backupconf['REMOTEIP'].":".$backupconf['DESTFLDR']."/".$userdata['FOLDER']."/".$userdata['USERNAME']."/homedir/mail ".$userhome[$userdata['USERNAME']]."/ ";
            $chowmcmd = "/bin/chown -R ".$userdata['USERNAME'].":".$userdata['USERNAME']." ".$userhome[$userdata['USERNAME']]."/mail";
            system( "{$restorecmd}" );
            system( "{$chowmcmd}" );
        }
        else if ( $userdata['TYPE'] == "homefolder" )
        {
            $logmsg = date( "r" )." : Restoring home  folders for the user ".$userdata['USERNAME']." from ".$userdata['FOLDER']." backup\n";
            error_log( "{$logmsg}", 3, "/var/log/cpremoterestore.log" );
            $restorecmd = $backupconf['RSYNCPTH']." -avz --delete -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$backupconf['REMOTEPRT']."' ".$backupconf['REMOTEUSR']."@".$backupconf['REMOTEIP'].":".$backupconf['DESTFLDR']."/".$userdata['FOLDER']."/".$userdata['USERNAME']."/homedir/  ".$userhome[$userdata['USERNAME']];
            $chowmcmd1 = "/bin/chown -R ".$userdata['USERNAME'].":".$userdata['USERNAME']." ".$userhome[$userdata['USERNAME']];
            $chowmcmd2 = "/bin/chown  ".$userdata['USERNAME'].":nobody  ".$userhome[$userdata['USERNAME']]."/public_html";
            $chowmcmd3 = "/bin/chown -R ".$userdata['USERNAME'].":mail  ".$userhome[$userdata['USERNAME']]."/etc";
            $chowmcmd4 = "/bin/chown  ".$userdata['USERNAME'].":nobody  ".$userhome[$userdata['USERNAME']]."/.htpasswds";
            system( "{$restorecmd}" );
            system( "{$chowmcmd1}" );
            system( "{$chowmcmd2}" );
            system( "{$chowmcmd3}" );
            system( "{$chowmcmd4}" );
        }
        else if ( $userdata['TYPE'] == "www" )
        {
            $logmsg = date( "r" )." : Restoring public_html/www  folders for the user ".$userdata['USERNAME']." from ".$userdata['FOLDER']." backup\n";
            error_log( "", 3, "/var/log/cpremoterestore.log" );
            $restorecmd = $backupconf['RSYNCPTH']." -avz --delete -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$backupconf['REMOTEPRT']."' ".$backupconf['REMOTEUSR']."@".$backupconf['REMOTEIP'].":".$backupconf['DESTFLDR']."/".$userdata['FOLDER']."/".$userdata['USERNAME']."/homedir/public_html  ".$userhome[$userdata['USERNAME']]."/";
            $chowmcmd1 = "/bin/chown -R ".$userdata['USERNAME'].":".$userdata['USERNAME']." ".$userhome[$userdata['USERNAME']]."/public_html";
            $chowmcmd2 = "/bin/chown  ".$userdata['USERNAME'].":nobody ".$userhome[$userdata['USERNAME']]."/public_html";
            system( "{$restorecmd}" );
            system( "{$chowmcmd1}" );
            system(  );
        }
        else if ( $userdata['TYPE'] == "full" )
        {
            $ufile = "/var/cpanel/users/".$userdata['USERNAME'];
            if ( file_exists( $ufile ) )
            {
                echo "The cPanel user ,".$userdata['USERNAME']." , is already available  in this server.  Please take a backup of this account to a secure location and terminate  this account , then try again to restore it. Don't place the backup in /home \n";
            }
            else
            {
                $logmsg = date( "r" )." : Restoring full account for the cPanel user ".$userdata['USERNAME']." from ".$userdata['FOLDER']."\n";
                error_log( "{$logmsg}", 3, "/var/log/cpremoterestore.log" );
                $restorecmd1 = "rm -rf /home/cpmove-".$userdata['USERNAME'];
                $restorecmd2 = "/bin/mkdir -p /home/cpmove-".$userdata['USERNAME'];
                $restorecmd3 = $backupconf['RSYNCPTH']." -avz --delete -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$backupconf['REMOTEPRT']."' ".$backupconf['REMOTEUSR']."@".$backupconf['REMOTEIP'].":".$backupconf['DESTFLDR']."/".$userdata['FOLDER']."/".$userdata['USERNAME']."/  /home/cpmove-".$userdata['USERNAME'];
                $restorecmd4 = "cd /home ; /bin/tar -cvzf cpmove-".$userdata['USERNAME'].".tar.gz cpmove-".$userdata['USERNAME']."/";
                $restorecmd6 = "/scripts/restorepkg ".$userdata['USERNAME'];
                system( "{$restorecmd1}" );
                system( "{$restorecmd2}" );
                system( "{$restorecmd3}" );
                system( "{$restorecmd4}" );
                chdir( "/root/" );
                system( "{$restorecmd1}" );
                system( "{$restorecmd6}" );
            }
        }
    }
    if ( $results['localkey'] )
    {
        $localkeydata = $results['localkey'];
        $cachekeyfile = "/etc/cpremote/localkey.txt";
        if ( !( $fh = fopen( $cachekeyfile, "w" ) ) )
        {
            exit( "can't open file" );
        }
        $stringData = $localkeydata;
        fwrite( $fh, $stringData );
        fclose( $fh );
    }
}
else if ( $results['status'] == "Invalid" )
{
    print " License  key not found. If you already have the license key, please update the key ";
}
else
{
    if ( $results['status'] == "Expired" )
    {
        print "Your License key  Expired . Please purchase the key and reactivate it.";
    }
    else
    {
        print "Your License key  is Suspended. Please contact  our support team from http://portal.syslint.com/supporttickets.php";
    }
}

?>
