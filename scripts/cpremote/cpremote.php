<?php

include( "lib.php" );
$lines = file( "/etc/cpremote/cpbackup.conf" );
foreach ( $lines as $key )
{
    $term = explode( ":", trim( $key ) );
    if ( $term[0] == "LICENSE" )
    {
        $key = $term[1];
        break;
        break;
    }
}
$licensekey = $key;
$lineskey = file( "/etc/cpremote/localkey.txt" );
$localkey = NULL;
foreach ( $lineskey as $key )
{
    $localkey .= $key;
}
$fvar10 = file( "/etc/wwwacct.conf" );
$IP = explode( " ", trim( $fvar10[0] ) );

$results['status'] = "Active";

if ( $results['status'] == "Active" )
{
    $cPremoteConf = array( );
    $fvar2 = file( "/etc/cpremote/cpbackup.conf" );
    $lic = explode( ":", trim( $fvar2[0] ) );
    $cPremoteConf['LICENSE'] = $lic[1];
    $remoteip = explode( ":", trim( $fvar2[1] ) );
    $cPremoteConf['REMOTEIP'] = $remoteip[1];
    $remoteusr = explode( ":", trim( $fvar2[2] ) );
    $cPremoteConf['REMOTEUSR'] = $remoteusr[1];
    $remoteport = explode( ":", trim( $fvar2[3] ) );
    $cPremoteConf['REMOTEPRT'] = $remoteport[1];
    $destfolder = explode( ":", trim( $fvar2[4] ) );
    $cPremoteConf['DESTFLDR'] = $destfolder[1];
    $rsyncpath = explode( ":", trim( $fvar2[5] ) );
    $cPremoteConf['RSYNCPTH'] = $rsyncpath[1];
    $status = explode( ":", trim( $fvar2[6] ) );
    $cPremoteConf['STATUS'] = $status[1];
    $bkpdaily = explode( ":", trim( $fvar2[7] ) );
    $cPremoteConf['BKPDAILY'] = $bkpdaily[1];
    $bkpweekly = explode( ":", trim( $fvar2[8] ) );
    $cPremoteConf['BKPWKLY'] = $bkpweekly[1];
    $bkpmonthly = explode( ":", trim( $fvar2[9] ) );
    $cPremoteConf['BKPMONTHLY'] = $bkpmonthly[1];
    $notice = explode( ":", trim( $fvar2[10] ) );
    $cPremoteConf['NOTIFICATION'] = $notice[1];
    $flinewww = file( "/etc/wwwacct.conf" );
    foreach ( $flinewww as $wwkey )
    {
        if ( preg_match( "/^CONTACTEMAIL/", $wwkey ) )
        {
            $x = explode( " ", $wwkey );
            $cemail = trim( $x[1] );
            $cPremoteConf['CONTACTEMAIL'] = $cemail;
        }
        if ( preg_match( "/^HOST/", $wwkey ) )
        {
            $x = explode( " ", $wwkey );
            $serverhn = trim( $x[1] );
            $cPremoteConf['HOSTNAME'] = $serverhn;
        }
    }
    if ( $handle = opendir( "/var/cpanel/users" ) )
    {
        while ( false !== ( $file = readdir( $handle ) ) )
        {
            if ( $file != "." && $file != ".." )
            {
                $_file_list .= $file.":";
            }
        }
        closedir( $handle );
    }
    $cPuser = explode( ":", $_file_list );
    sort( $cPuser );
    $ArrLen = count( $cPuser );
    $cpUsernumber = $ArrLen - 1;
    $hlines = file( "/etc/cpremote/home.list" );
    $userhome = array( );
    $userphome = array( );
    foreach ( $hlines as $key )
    {
        $term = explode( ":", trim( $key ) );
        $userhome[$term[0]] = $term[1];
        $term2 = explode( "/", $userhome[$term[0]] );
        $userphome[$term[0]] = trim( $term2[1] );
    }
    $ulines = file( "/etc/cpremote/userhomes.txt" );
    $storehome = array( );
    foreach ( $ulines as $key )
    {
        $storehome[] = trim( $key );
    }
    if ( $cPremoteConf['STATUS'] == "1" )
    {
        echo "Backup seems enabled";
        if ( $cPremoteConf['BKPDAILY'] == "1" )
        {
            echo " Daily backup ";
            $createfolder = $Tmp_328."@".$cPremoteConf['REMOTEIP']." /bin/mkdir -p ".$cPremoteConf['DESTFLDR']."/daily";
            $cmdcreateDest = system( $createfolder, $op );
            $d = date( "d-M-Y" );
            uptodate( "/etc/cpremote/status/daily.log", $d );
            $i = 1;
            while ( $i <= $cpUsernumber )
            {
                if ( file_exists( "/etc/cpremote/status/{$cPuser[$i]}" ) )
                {
                    echo " backup is disabled for user ".$cPuser[$i];
                }
                else
                {
                    echo "\n================================================================================\n";
                    echo "                          Taking daily backup of user {$cPuser[$i]} \n";
                    echo "================================================================================\n";
                    $createBkp = "/scripts/pkgacct --skiphomedir ".$cPuser[$i];
                    putenv( "INCBACKUP=1" );
                    $rescreateBkp = system( $createBkp, $op );
                    $q = 0;
                    while ( $q < count( $storehome ) )
                    {
                        $_tmh = $storehome[$q]."/cpmove-".$cPuser[$i];
                        if ( file_exists( $_tmh ) )
                        {
                            $STORE = $_tmh;
                            break;
                        }
                        $STORE = "/home/cpmove-".$cPuser[$i];
                        ++$q;
                    }
                    $rmTmpbkp = "rm -rf ".$STORE;
                    $syncBkp = $cPremoteConf['RSYNCPTH']." -avz  -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$cPremoteConf['REMOTEPRT']."'  ".$STORE."/* ".$cPremoteConf['REMOTEUSR']."@".$cPremoteConf['REMOTEIP'].":".$cPremoteConf['DESTFLDR']."/daily/".$cPuser[$i]."/";
                    $cmdCrhomedir = "/bin/mkdir  ".$STORE."/homedir";
                    $cmdSyncHome = $cPremoteConf['RSYNCPTH']." -avz --delete -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$cPremoteConf['REMOTEPRT']."'  ".$userhome[$cPuser[$i]]."/ ".$cPremoteConf['REMOTEUSR']."@".$cPremoteConf['REMOTEIP'].":".$cPremoteConf['DESTFLDR']."/daily/".$cPuser[$i]."/homedir";
                    if ( file_exists( $userhome[$cPuser[$i]] ) )
                    {
                        $rescreatehomedir = system( $cmdCrhomedir, $op );
                        $resSyncBkp = system( $syncBkp, $op );
                        $rsynchomedir = system( $cmdSyncHome, $op );
                        $resrmTmbpk = system( $rmTmpbkp, $op );
                    }
                }
                ++$i;
            }
        }
        if ( $cPremoteConf['BKPWKLY'] != "0" )
        {
            $today = date( "N" );
            if ( $cPremoteConf['BKPWKLY'] == $today )
            {
                echo "Taking weekly backup";
                $createfolder = "/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$cPremoteConf['REMOTEPRT']." ".$cPremoteConf['REMOTEUSR']."@".$cPremoteConf['REMOTEIP']." /bin/mkdir -p ".$cPremoteConf['DESTFLDR']."/weekly";
                $cmdcreateDest = system( $createfolder, $op );
                $d = date( "d-M-Y" );
                uptodate( "/etc/cpremote/status/weekly.log", $d );
                $i = 1;
                while ( $i <= $cpUsernumber )
                {
                    if ( file_exists( "/etc/cpremote/status/{$cPuser[$i]}" ) )
                    {
                        echo " backup is disabled for user ".$cPuser[$i];
                    }
                    else
                    {
                        echo "\n================================================================================\n";
                        echo "                          Taking weekly backup of user {$cPuser[$i]} \n";
                        echo "================================================================================\n";
                        $createBkp = "/scripts/pkgacct --skiphomedir ".$cPuser[$i];
                        putenv( "INCBACKUP=1" );
                        $rescreateBkp = system( $createBkp, $op );
                        $q = 0;
                        while ( $q < count( $storehome ) )
                        {
                            $_tmh = $storehome[$q]."/cpmove-".$cPuser[$i];
                            if ( file_exists( $_tmh ) )
                            {
                                $STORE = $_tmh;
                                break;
                            }
                            $STORE = "/home/cpmove-".$cPuser[$i];
                            ++$q;
                        }
                        $rmTmpbkp = "rm -rf ".$STORE;
                        $syncBkp = $Tmp_613.$cPremoteConf['REMOTEPRT']."'  ".$STORE."/* ".$cPremoteConf['REMOTEUSR']."@".$cPremoteConf['REMOTEIP'].":".$cPremoteConf['DESTFLDR']."/weekly/".$cPuser[$i]."/";
                        $cmdCrhomedir = "/bin/mkdir  ".$STORE."/homedir";
                        $cmdSyncHome = $cPremoteConf['RSYNCPTH']." -avz --delete -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$cPremoteConf['REMOTEPRT']."'  ".$userhome[$cPuser[$i]]."/ ".$Var_15840."@".$cPremoteConf['REMOTEIP'].":".$cPremoteConf['DESTFLDR']."/weekly/".$cPuser[$i]."/homedir";
                        if ( file_exists( $userhome[$cPuser[$i]] ) )
                        {
                            $rescreatehomedir = system( $cmdCrhomedir, $op );
                            $resSyncBkp = system( $syncBkp, $op );
                            $rsynchomedir = system( $cmdSyncHome, $op );
                            $resrmTmbpk = system( $rmTmpbkp, $op );
                        }
                    }
                    ++$i;
                }
            }
        }
        if ( $cPremoteConf['BKPMONTHLY'] == "1" )
        {
            $today = date( "j" );
            if ( $cPremoteConf['BKPMONTHLY'] == $today )
            {
                $createfolder = "/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$cPremoteConf['REMOTEPRT']." ".$cPremoteConf['REMOTEUSR']."@".$cPremoteConf['REMOTEIP']." /bin/mkdir -p ".$cPremoteConf['DESTFLDR']."/monthly";
                $cmdcreateDest = system( $createfolder, $op );
                $d = date( "d-M-Y" );
                uptodate( "/etc/cpremote/status/monthly.log", $d );
                echo "Talking Monthly backup";
                $i = 1;
                while ( $i <= $cpUsernumber )
                {
                    if ( file_exists( "/etc/cpremote/status/{$cPuser[$i]}" ) )
                    {
                        echo " backup is disabled for user ".$cPuser[$i];
                    }
                    else
                    {
                        echo "\n================================================================================\n";
                        echo "                          Taking Monthly backup of user {$cPuser[$i]} \n";
                        echo "================================================================================\n";
                        $createBkp = "/scripts/pkgacct --skiphomedir ".$cPuser[$i];
                        putenv( "INCBACKUP=1" );
                        $rescreateBkp = system( $createBkp, $op );
                        $q = 0;
                        while ( $q < count( $storehome ) )
                        {
                            $_tmh = $storehome[$q]."/cpmove-".$cPuser[$i];
                            if ( file_exists( $_tmh ) )
                            {
                                $STORE = $_tmh;
                                break;
                            }
                            $STORE = "/home/cpmove-".$cPuser[$i];
                            ++$q;
                        }
                        $rmTmpbkp = "rm -rf ".$STORE;
                        $syncBkp = $cPremoteConf['RSYNCPTH']." -avz  -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$cPremoteConf['REMOTEPRT']."'  ".$STORE."/* ".$cPremoteConf['REMOTEUSR']."@".$cPremoteConf['REMOTEIP'].":".$cPremoteConf['DESTFLDR']."/monthly/".$cPuser[$i]."/";
                        $cmdCrhomedir = "/bin/mkdir  ".$STORE."/homedir";
                        $cmdSyncHome = $cPremoteConf['RSYNCPTH']." -avz --delete -e '/usr/bin/ssh -F /etc/cpremote/ssh_config -p ".$cPremoteConf['REMOTEPRT']."'  ".$userhome[$cPuser[$i]]."/ ".$cPremoteConf['REMOTEUSR']."@".$cPremoteConf['REMOTEIP'].":".$cPremoteConf['DESTFLDR']."/monthly/".$cPuser[$i]."/homedir";
                        if ( file_exists( $userhome[$cPuser[$i]] ) )
                        {
                            $rescreatehomedir = system( $cmdCrhomedir, $op );
                            $resSyncBkp = system( $syncBkp, $op );
                            $rsynchomedir = system( $cmdSyncHome, $op );
                            $resrmTmbpk = system( $rmTmpbkp, $op );
                        }
                    }
                    ++$i;
                }
            }
        }
        if ( $cPremoteConf['NOTIFICATION'] == "1" )
        {
            $sendNotice = notification( $cPremoteConf['CONTACTEMAIL'], $cPremoteConf['HOSTNAME'] );
        }
    }
    else
    {
        echo " Backup not enabled";
    }
    if ( $results['localkey'] )
    {
        $localkeydata = $results['localkey'];
        $cachekeyfile = "/etc/cpremote/localkey.txt";
        if ( !( $fh = fopen( $cachekeyfile, "w" ) ) )
        {
            exit( "can't open file" );
        }
        $stringData = $localkeydata;
        fwrite( $fh, $stringData );
        fclose( $fh );
    }
}
else if ( $results['status'] == "Invalid" )
{
    print "\tLicense  key not found. If you already have the license key, please update the key ";
}
else
{
    if ( $results['status'] == "Expired" )
    {
        print "Your License key  Expired . Please purchase the key and reactivate it.";
    }
    else
    {
        if ( $results['status'] == "Suspended" )
        {
            print "Your License key is Suspended";
        }
    }
}
?>