#!/bin/bash
export REMOTVERSION=$( /usr/bin/curl http://valroot.com/cpbackup/version.txt )
export LOCALVERSION=$(cat /etc/cpremote/version.txt)

echo "Remote version $REMOTVERSION"
echo "Local version $LOCALVERSION"
if [ $REMOTVERSION ==  $LOCALVERSION ]; then 
        echo "Same version. It is uptodate"
else
        rm -rf /usr/local/src/cpremote*
        cd /usr/local/src
        wget -c http://portal.syslint.com/downloads/cpremote.$REMOTVERSION.tar.gz
        tar -xzf cpremote.$REMOTVERSION.tar.gz
        cd cpremote/
        sh install.sh
        echo "Upgrade completed"
fi

